# What is this?
It's like my other project,
[halite-native-visualiser](https://gitlab.cecs.anu.edu.au/u5586058/halite_native_visualiser)
but written in rust instead of C++.

# How do I build/run it?
First, you need the sdl2 library.
[Install rust](https://rustup.rs/),
then:
```
$ cargo run --release
```