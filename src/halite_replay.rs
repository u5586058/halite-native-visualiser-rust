extern crate json;

#[derive(Debug)]
pub struct HaliteReplay {
    raw_data: json::JsonValue,
}

impl From<json::JsonValue> for HaliteReplay {
    fn from(raw_data: json::JsonValue) -> HaliteReplay {
        HaliteReplay { raw_data: raw_data }
    }
}

impl HaliteReplay {
    pub fn num_players(&self) -> Option<u32> {
        self.raw_data["num_players"].as_u32()
    }

    pub fn num_frames(&self) -> Option<u32> {
        self.raw_data["num_frames"].as_u32()
    }

    pub fn width_height(&self) -> Option<(u32, u32)> {
        match (self.raw_data["width"].as_u32(),
               self.raw_data["height"].as_u32()) {
            (_, None)                   => None,
            (None, _)                   => None,
            (Some(width), Some(height)) => Some((width, height)),
        }
    }

    pub fn get_frame(&self, frame_no: u32) -> &json::JsonValue {
        &self.raw_data["frames"][frame_no as usize]
    }

    pub fn get_productions(&self) -> &json::JsonValue {
        &self.raw_data["productions"]
    }
}
