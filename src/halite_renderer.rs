extern crate json;
extern crate sdl2;

use sdl2::pixels::Color;
use sdl2::rect::Rect;

pub type ColorSpec = (u8, u8, u8);
const SCALE_FACTOR: u32   = 20;

// this should probably be a macro
// todo: Move to nightly and use const fn
#[inline]
fn to_rgb(color: ColorSpec) -> Color {
    let (r, g, b) = color;
    Color::RGB(r, g, b)
}

#[inline]
fn scale_color(color: ColorSpec, scale: u32) -> Color {
    let (r, g, b) = color;
    Color::RGB((r as u32 * scale / SCALE_FACTOR) as u8,
               (g as u32 * scale / SCALE_FACTOR) as u8,
               (b as u32 * scale / SCALE_FACTOR) as u8)
}

const BLACK_COLOR: Color  = Color::RGB(0x00, 0x00, 0x00);
const SITE_WIDTH: usize   = 20;
const SITE_HEIGHT: usize  = 20;

pub struct RendererLuts {
    strength_luts: [(usize, usize); 256],
}

impl RendererLuts {
    pub fn new() -> RendererLuts {
        // precompute the luts
        let mut sl = RendererLuts { strength_luts: [(0, 0); 256] };

        for (index, val) in sl.strength_luts.iter_mut().enumerate() {
            let inverse_index = u8::max_value() as usize - index;
            let offset_h = ((SITE_WIDTH / 2) * inverse_index) / u8::max_value() as usize;
            let offset_v = ((SITE_HEIGHT  / 2) * inverse_index) / u8::max_value() as usize;
            *val = (offset_h, offset_v);
        }

        sl
    }
}

pub fn clear_frame(renderer: &mut sdl2::render::Renderer) {
    renderer.set_draw_color(BLACK_COLOR);
    renderer.fill_rect(None).expect("Filling in the bg failed!");
}

pub fn render_frame(renderer: &mut sdl2::render::Renderer,
                    frame: &json::JsonValue,
                    productions: &json::JsonValue,
                    renderer_luts: &RendererLuts,
                    player_colors: &Vec<ColorSpec>) -> Result<(), String> {
    clear_frame(renderer);

    assert!(frame.is_array());
    for (y, row) in frame.members().enumerate() {
        assert!(row.is_array());
        for (x, site) in row.members().enumerate() {
            let owner = site[0].as_usize().unwrap();
            let strength = site[1].as_usize().unwrap();
            let color = player_colors[owner];

            let color_scaled = scale_color(color, productions[y][x].as_u32().unwrap());
            renderer.set_draw_color(color_scaled);
            try!(renderer.fill_rect(Rect::new((x * SITE_WIDTH) as i32,
                                              (y * SITE_HEIGHT) as i32,
                                              SITE_WIDTH as u32,
                                              SITE_HEIGHT as u32)));
            let offsets;
            unsafe {
                offsets = *renderer_luts.strength_luts.get_unchecked(strength);
            }
            let (offset_h, offset_v) = offsets;
            let color_full = to_rgb(color);
            renderer.set_draw_color(color_full);
            try!(renderer.fill_rect(Rect::new(((x * SITE_WIDTH) + offset_h) as i32,
                                              ((y * SITE_HEIGHT) + offset_v) as i32,
                                              (SITE_WIDTH - (2 * offset_h)) as u32,
                                              (SITE_HEIGHT - (2 * offset_v)) as u32)));
        }
    }

    Ok(())
}
