extern crate json;
extern crate sdl2;
extern crate rand;

mod halite_replay;
mod halite_renderer;

use std::env;
use std::process;
use std::fs::File;
use std::io::prelude::*;
use halite_replay::HaliteReplay;
use halite_renderer::ColorSpec;
use sdl2::event::Event;
use rand::Rng;

const DEFAULT_REPLAY_FILENAME: &'static str = "replay.hlt";
const WINDOW_TITLE: &'static str = "Halite Replay Visualiser";
const WINDOW_WIDTH: u32   = 1920;
const WINDOW_HEIGHT: u32  = 1080;
const DEFAULT_FPS: u32    =    5;

const WHITE: ColorSpec    = (0xff, 0xff, 0xff);
const RED: ColorSpec      = (0xff, 0x00, 0x00);
const GREEN: ColorSpec    = (0x00, 0xff, 0x00);
const BLUE: ColorSpec     = (0x00, 0x00, 0xff);
const COLOR_CHOICES: [ColorSpec; 3] = [RED, GREEN, BLUE];

fn print_usage_exit(exec_name: &String) {
    println!("Usage: {} <replay filename>", exec_name);
    process::exit(1);
}

fn get_file_contents(filename: &String) -> Result<String, std::io::Error> {
    let mut f = try!(File::open(filename.as_str()));

    let mut s = String::new();
    try!(f.read_to_string(&mut s));

    Ok(s)
}

fn get_replay(filename: &String) -> Result<HaliteReplay, String> {
    get_file_contents(filename)
   .map_err(|e| e.to_string())
   .and_then(|contents|
        json::parse(contents.as_str())
       .map_err(|e| e.to_string())
       .and_then(|json| Ok(HaliteReplay::from(json))))
}

fn get_player_colors(num_players: u32) -> Vec<ColorSpec> {
    if num_players as usize > COLOR_CHOICES.len() {
        panic!("More players in the replay than available color choices!");
    }

    let mut rng = rand::thread_rng();

    let color_options = COLOR_CHOICES;
    let mut color_choices = Vec::new();
    for choice in color_options.iter() {
        color_choices.push(*choice);
    }

    // player 0 is neutral, this must be white
    let mut player_colors = vec![WHITE];
    for _ in 0..num_players {
        // pick a random color from the choices we have remaining
        let pick = rng.gen_range(0, color_choices.len());
        player_colors.push(color_choices[pick]);
        color_choices.remove(pick);
    }

    player_colors
}

fn main() {
    let mut args = env::args();

    // Read in command line args
    // todo: rework this with a nicer interface
    let exec_name = args.next().unwrap();
    let replay_filename = match args.next() {
        Some(s) => s,
        None    => DEFAULT_REPLAY_FILENAME.to_string(),
    };

    if args.next() != None {
        print_usage_exit(&exec_name);
    }

    println!("Loading replay {}", replay_filename);
    let replay = match get_replay(&replay_filename) {
        Err(e) => {
            println!("Error reading replay: {}", e);
            process::exit(1);
        },
        Ok(replay) => replay,
    };
    println!("Done!");

    let num_players = replay.num_players()
                            .expect("Couldn't read number of players from json");
    println!("There are {} players in this replay", num_players);
    let player_colors = get_player_colors(num_players);

    let num_frames = replay
                    .num_frames()
                    .expect("Couldn't read number of frames from json");
    println!("There are {} frames in this replay", num_frames);

    let (width, height) = replay.width_height()
                                .expect("Couldn't read the width and height from json");
    println!("The board size is {} by {}", width, height);

    let fps = DEFAULT_FPS;
    let mut frame_ms = 1000 / fps;
    // maximum 1ms per frame
    if frame_ms == 0 {
        frame_ms = 1;
    }

    let ctx = sdl2::init().unwrap();
    let mut events = ctx.event_pump().unwrap();
    let video_ctx = ctx.video().unwrap();
    let mut timer = ctx.timer().unwrap();

    // setup the luts
    let renderer_luts = halite_renderer::RendererLuts::new();

    let mut window = video_ctx.window(WINDOW_TITLE, WINDOW_WIDTH, WINDOW_HEIGHT)
                              .position_centered()
                              .opengl()
                              .build()
                              .unwrap();
    window.show();

    let mut renderer = window.renderer().build().unwrap();
    println!("Renderer: {}", renderer.info().name);

    halite_renderer::render_frame(&mut renderer,
                                  replay.get_frame(0),
                                  replay.get_productions(),
                                  &renderer_luts,
                                  &player_colors).unwrap();
    renderer.present();
    let mut next_frame_time = timer.ticks() + frame_ms;

    'event: for frame_no in 1..num_frames {
        // will panic if rendering fails
        halite_renderer::render_frame(&mut renderer,
                                      replay.get_frame(frame_no),
                                      replay.get_productions(),
                                      &renderer_luts,
                                      &player_colors).unwrap();
        renderer.present();
        let current_time = timer.ticks();
        if next_frame_time > current_time {
            let until_next_frame = next_frame_time - current_time;
            timer.delay(until_next_frame);
            next_frame_time += frame_ms;
        } else {
            println!("Not able to reach target framerate!");
            println!("Target: {} Actual: {} ", fps, 1000 / (frame_ms + current_time - next_frame_time));
            next_frame_time = current_time + frame_ms;
        }
        for event in events.poll_iter() {
            match event {
                Event::Quit{..} => break 'event,
                _               => continue,
            }
        }
    }
}
